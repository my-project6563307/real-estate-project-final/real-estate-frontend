$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");

        var urlInfo = "http://localhost:8080/realestate/api/test/me";

        var gUserUrl = "http://localhost:8080/realestate/employees/me";

        var gPasswordUpdateUrl = "http://localhost:8080/realestate/employees/old-password/";

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();

        $("#btn-add-customer").on("click", function(){
            onBtnAddCustomerClick();
        })

        //Set sự kiện cho nút logout
        $(document).on("click", ".btn-out", function(){
            redirectToLogin();
        });
        //Set sự kiện cho nút password
        $(document).on("click", "#btn-change-password", function(){
            $("#block-password").removeClass("d-none");;
        });

        //Set sự kiện cho nút cancel change password
        $(document).on("click", "#btn-cancel", function(){
            $("#block-password").addClass("d-none");
        });

        //Set sự kiện cho nút confirm change password
        $(document).on("click", "#btn-confirm-change", function(){
           onBtnChangePassword();
        });

        // Xử lí lấy ảnh
        $("#inp-photo").change(function(e) {
            $("#upload-photo").empty();
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
        
                var file = e.originalEvent.srcElement.files[i];
        
                var img = document.createElement("img");
                img.setAttribute("id", "photo-upload");
                var reader = new FileReader();
                reader.onloadend = function() {
                     img.src = reader.result;
                }
                reader.readAsDataURL(file);
                $("#upload-photo").html(img);
            }
        });

        //Thêm sự kiện nhất nút lưu thông tin người dùng
        $(document).on("click", "#btn-save-user", function(){
            var vUserObj = {
                firstName: "",
                lastName: "",
                homePhone: "",
                birthDate: "",
                address: "",
                country: "",
                region: "",
                city: "",
                title: "",
                notes: "",
                photo: "",
                email: ""
            }
            // Thu thập dữ liệu
            readUserInfor(vUserObj);
            console.log(vUserObj);
            // Gọi api cập nhật thông tin người dùng
            callApiUpdateUser(vUserObj);
        });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiCheckUser();
        }

        // Hàm thay đổi mật khẩu
        function onBtnChangePassword(){
            "use strict"
            console.log("aa");
            var vPasswordObj = {
                oldPassword: "",
                newPassWord:"",
                repeatPassWord:""
            }

            // Thu thập dữ liệu
            readPasswordData(vPasswordObj);
            // Kiểm tra dữ liệu
            if (validatePassword(vPasswordObj)){
                callApiChangePassword(vPasswordObj);
            }
        }
        // Hàm kiểm tra người dùng đăng nhập
        function callApiCheckUser(){
            $.ajax({
                url: urlInfo,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                },
                success: function(responseObject) {
                    console.log(responseObject);
                    responseHandler(responseObject);
                },
                error: function(xhr) {
                    console.log(xhr);
                }
            });
        }
        // Hàm gọi api cập nhật user
        function callApiUpdateUser(paramObj){
            "use strict"
            var vJsonUser =JSON.stringify(paramObj);
            $.ajax({
                url: gUserUrl,
                type: "PUT",
                contentType: "application/json",
                data: vJsonUser,
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                },
                success: function(responseObject) {
                    console.log(responseObject);
                    alert("Successfully!")
                },
                error: function(xhr) {
                    console.log(xhr);
                }
            });
        }
        // Hàm gọi api cập nhật user
        function callApiChangePassword(paramObj){
            "use strict"
            $.ajax({
                url: gPasswordUpdateUrl + paramObj.oldPassword 
                + "/new-password/" + paramObj.newPassWord,
                type: "PUT",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                },
                success: function(responseObject) {
                    console.log(responseObject);
                    alert("Successfully!")
                },
                error: function(xhr) {
                    console.log(xhr);
                    alert(xhr.responseText);
                }
            });
        }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    // Hàm xử lí sau khi check đăng nhập user 
    function responseHandler(res){
        "use strict"

        $("#username").html(res.username);
        $("#email").html(res.email);

        $("#inp-firstname").val(res.firstName);
        $("#inp-lastname").val(res.lastName);
        $("#inp-phone").val(res.homePhone);
        $("#inp-birthday").val(res.birthDate);
        $("#inp-address").val(res.address);
        $("#inp-country").val(res.country);
        $("#inp-city").val(res.city);
        $("#inp-region").val(res.region);
        $("#inp-title").val(res.title);
        $("#inp-note").val(res.notes);
        $("#inp-email").val(res.email);
        $("#photo-upload").attr("src", res.photo);

        if(res.photo != null){
            $("#profile-avatar").attr("src", res.photo);
            $("#user-avatar").attr("src", res.photo);
        }

        $("#list-item-sidebar").empty();
        $(".action-block").empty();

        var vUser = `<p class="desc">`+ res.username +`</p>
        <hr>
        <a href = "#!" class="real-estate-manage login-title">Your real estates</a>
        <hr>
        <a class="btn-out signout-title">Sign-out</a>`

        var vSidebarItem = `         
            <li><p class="desc">`+ res.username +`</p></li>
            <a href = "#!" class="real-estate-manage">Your real estates</a>
            <li><a href="./real-estate-add.html" class="btn-add-property">Add real estate</a></li>
            <hr>
            <li><a href="#">Home</a></li>
            <li><a href="#footer">About</a></li>
            <li><a class="btn-out signout-sidebar">Sign out</a></li>`

        $(".action-block").html(vUser);
        $("#list-item-sidebar").html(vSidebarItem);
        $("#btn-add-property").removeClass("d-none");

        if (res.roles[0].name == "ROLE_CUSTOMER"){
            $(".real-estate-manage").attr("href", "./customer-manage.html");
        }
        if (res.roles[0].name == "ROLE_HOME_SELLER"){
            $(".real-estate-manage").attr("href", "./home-seller-manage.html");
        }
        if (res.roles[0].name == "ROLE_ADMIN"){
            $(".real-estate-manage").attr("href", "./admin-manage.html");
        }
    }

    // Hàm thu thập dữ liệu
    function readUserInfor(paramObj){
        "use strict"
        paramObj.firstName = $("#inp-firstname").val().trim();
        paramObj.lastName = $("#inp-lastname").val().trim();
        paramObj.homePhone = $("#inp-phone").val().trim();
        paramObj.birthDate = $("#inp-birthday").val().trim();
        paramObj.address = $("#inp-address").val().trim();
        paramObj.country = $("#inp-country").val().trim();
        paramObj.city = $("#inp-city").val().trim();
        paramObj.region = $("#inp-region").val().trim();
        paramObj.title = $("#inp-title").val().trim();
        paramObj.notes = $("#inp-note").val().trim();
        paramObj.email = $("#inp-email").val().trim();
        paramObj.photo = $("#photo-upload").attr("src");
    }

    // Ham thu thập dữ liệu password
    function readPasswordData(paramObj){
        "use strict"
        paramObj.oldPassword = $("#inp-old-password").val();
        paramObj.newPassWord = $("#inp-new-password").val();
        paramObj.repeatPassWord = $("#inp-repeat-password").val();
    }

    // Hàm kiểm tra password
    function validatePassword(paramObj){
        "use strict"
        if (paramObj.oldPassword == "" || paramObj.newPassWord == "" || paramObj.repeatPassWord == ""){
            alert("Please enter password!")
            return false;
        }
        if (paramObj.newPassWord.length < 7){
            alert("New password must more 7 characters!");
            return false;
        }
        if (paramObj.newPassWord != paramObj.repeatPassWord){
            alert("New password not matched")
            return false;
        }
        return true;
    }
})