$(document).ready(function() {
    //----Phần này làm sau khi đã làm trang info.js
    //Kiểm tra token nếu có token tức người dùng đã đăng nhập
    const vtoken = getCookie("token");

    if(vtoken) {
        window.location.href = "index.html";
    }
    //----Phần này làm sau khi đã làm trang info.js
    //Sự kiện bấm nút login
    $("#btn-signup").on("click", function() {      

        var vObj = {
            username: "",
            password: "",
            email: "",
            repeatPassword: "",
            isAgree: false,
        }
        vObj.username = $("#inp-username").val().trim();
        vObj.email = $("#inp-email").val().trim();
        vObj.password = $("#inp-password").val().trim();
        vObj.repeatPassword = $("#inp-repeat-password").val().trim();
        vObj.isAgree = $("#inp-check").is(":checked");

        var vValid  = validateData(vObj);
        console.log(vObj);
        if(vValid){
            showError("");
            signinForm(vObj);
        }

    });

    function signinForm(paramObj) {
        var vSignupUrl = "http://localhost:8080/realestate/api/test/signup";

        var vJsonObj = JSON.stringify(paramObj);

        $.ajax({
            type: "POST",
            url: vSignupUrl,
            data: vJsonObj, 
            contentType: "application/json",
            cache : false,
            processData: false,
            success: function(res){
                alert(res.message);
                window.location.href = "./login.html"
            },
            error: function(xhr) {
               // Lấy error message
                console.log(xhr.responseText);
               showError("xhr.responseText");
            }
        });
    }

    //Xử lý object trả về khi login thành công
    function responseHandler(data) {
        //Lưu token vào cookie

        setCookie("token", data.accessToken);
            window.location.href = "index.html";
    }

    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }

    //Hiển thị lỗi lên form 
    function showError(message) {
        var errorElement = $("#error");
        errorElement.html(message);
        errorElement.addClass("d-block");
        errorElement.addClass("d-none");
    }

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    // Hàm xử lí dữ liệu
    function validateData(vObj){
        "use strict"
        if (vObj.username == ""){
            showError("Please enter username!");
            return false;
        }
        if (vObj.email == ""){
            showError("Please enter email!");
            return false;
        }
        if (vObj.password == ""){
            showError("Please enter password!");
            return false;
        }
        if (vObj.repeatPassword == ""){
            showError("Please enter repeat password!");
            return false;
        }
        if (vObj.password != vObj.repeatPassword){
            showError("Password not matched!");
            return false;
        }
        if (vObj.password.length < 7){
            showError("Pass word too short!");
            return false;
        }
        if (vObj.isAgree == false){
            showError("You need to agree terms and policies!");
            return false;
        }
        return true;
    }
});