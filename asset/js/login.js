$(document).ready(function() {
    //----Phần này làm sau khi đã làm trang info.js
    //Kiểm tra token nếu có token tức người dùng đã đăng nhập
    const vtoken = getCookie("token");

    if(vtoken) {
        window.location.href = "index.html";
    }
    //----Phần này làm sau khi đã làm trang info.js
    //Sự kiện bấm nút login
    $("#btn-login").on("click", function() {      

        var vObj = {
            username: "",
            password: ""
        }
        vObj.username = $("#inp-username").val().trim();
        vObj.password = $("#inp-password").val().trim();
        signinForm(vObj);
    });

    function signinForm(paramObj) {
        var vloginUrl = "http://localhost:8080/realestate/api/auth/signin";

        var vJsonObj = JSON.stringify(paramObj);

        $.ajax({
            type: "POST",
            url: vloginUrl,
            data: vJsonObj, 
            contentType: "application/json",
            cache : false,
            processData: false,
            success: function(responseObject) {
                console.log(responseObject);
                responseHandler(responseObject);
            },
            error: function(xhr) {
               // Lấy error message
                console.log(xhr.responseText);
               showError("Login error! Please check your username and password");
            }
        });
    }

    //Xử lý object trả về khi login thành công
    function responseHandler(data) {
        //Lưu token vào cookie

        setCookie("token", data.accessToken);
            window.location.href = "index.html";
    }

    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }

    //Hiển thị lỗi lên form 
    function showError(message) {
        var errorElement = $("#error");
        errorElement.html(message);
        errorElement.addClass("d-block");
        errorElement.addClass("d-none");
    }

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
});