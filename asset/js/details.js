$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");

        var gUrl = new URL(window.location.href);

        var gId = gUrl.searchParams.get("id");

        var gRealEsateUrl = "http://localhost:8080/realestate/api/test/real_estates/posted";
    
        var gCustomerUrL = "http://localhost:8080/realestate/api/test/customers";

        var urlInfo = "http://localhost:8080/realestate/api/test/me";
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();

        $("#btn-add-customer").on("click", function(){
            onBtnAddCustomerClick();
        })

        //Set sự kiện cho nút logout
        $(document).on("click", ".btn-out", function(){
            console.log("abc");
            redirectToLogin();
        });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiCheckUser();
            callApiGetRealEstateById(gId);
        }
        
        // Hàm xử lí nhấn nút add customer
        function onBtnAddCustomerClick(){
            "use strict"
            var vCustomerInfo = {
                contactName: "",
                email: "",
                mobile: "",
                address: "",
                contactTitle: "",
                note: ""
            }

            // Read data customer form 
            readDataCustomerForm(vCustomerInfo);
            // Gọi api thêm customer vào database
            callApiAddCustomer(vCustomerInfo);

        }

        // Hàm kiểm tra người dùng đăng nhập
        function callApiCheckUser(){
            $.ajax({
                url: urlInfo,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                },
                success: function(responseObject) {
                    console.log(responseObject);
                    responseHandler(responseObject);
                },
                error: function(xhr) {
                    console.log(xhr);
                }
            });
        }
        // Hàm gọi api lấy real estate
        function callApiGetRealEstateById(paramId){
            "use strict"
            $.ajax({
                url: gRealEsateUrl  + "/" + paramId,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                async: false,
                success: function(res){
                    console.log(res);
                    loadRealEstateToContent(res);
                },
                error: function(er){
                    console.log(er.responseText);
                }
            })
        }
        // Hàm gọi api add customer
        function callApiAddCustomer(paramObj){
            "use strict"
            var vJsonCustomerObj = JSON.stringify(paramObj);
            $.ajax({
                url: gCustomerUrL,
                type: "POST",
                contentType: "application/json",
                data: vJsonCustomerObj,
                success: function(res){
                    console.log(res);
                    alert("SUCCESS");
                },
                error: function(er){
                    console.log(er.responseText);
                }
            })
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    function loadRealEstateToContent(paramRes){
        "use strict"
        var vContentWrap = $("#content-realestate");

        vContentWrap.empty();

        var vContent = `
        <img src="` + paramRes.photo + `" alt="" class="estate-img">
        <div class="list">
            <h2 class="title">` + paramRes.title + `</h2>
            <p class="desc">Apartment Code: ` + paramRes.apart_code + `</p>
            <p class="desc">Acreage: ` + paramRes.acreage + `</p>
            <p class="desc">Numbers of bedrooms: ` + paramRes.bedroom + `</p>
            <p class="desc">Numbers of baths : ` + paramRes.bath + `</p>
            <p class="desc">Numbers of floors: ` + paramRes.number_floors + `</p>
        </div>`
        
        vContentWrap.append(vContent);

        var vContentProjectWrap = $("#content-project");
        vContentProjectWrap.empty();
        var vContentProject = `<h2 class="title">In Project: `+ paramRes.project.name +`</h2>
        <p class="address">` + paramRes.project.address  +", " + paramRes.project.ward.name + ", " + paramRes.project.province.name + `</p>
        <p class="desc">`+ paramRes.project.description +`</p>
        <div class="body">
            <img src="`+ paramRes.project.photo +`" alt="" class="project-img">
            <ul class="project-feature">
                <h2 class="sub-title">Features:</h2>
                <li><p class="desc"> <b>Acreage: </b>`+ paramRes.project.acreage +`</p></li>
                <li><p class="desc"> <b>Number apartment: </b>`+ paramRes.project.numApartment +`</p></li>
                <li><p class="desc"> <b>Number block: </b>`+ paramRes.project.numBlock +`</p></li>
                <li><p class="desc"> <b>Number floors: </b>`+ paramRes.project.numFloors +`</p></li>
                <li><p class="desc"> <b>Design unit: </b>`+ paramRes.project.designUnit.name +`</p></li>
                <li><p class="desc"> <b>Investor: </b>`+ paramRes.project.investor.name +`</p></li>
            </ul>
        </div>`
        
        vContentProjectWrap.append(vContentProject);
    }

    // Hàm thu thập dữ liệu
    function readDataCustomerForm(paramObj){
        "use strict"
        paramObj.contactName = $("#inp-name").val();
        paramObj.email = $("#inp-email").val();
        paramObj.mobile = $("#inp-mobile").val();
        paramObj.address = $("#inp-address").val();
        paramObj.contactTitle = $("#inp-title").val();
        paramObj.note = $("#inp-note").val();
    }

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    // Hàm xử lí sau khi check đăng nhập user 
    function responseHandler(res){
        "use strict"
        if(res.photo != null){
            $("#user-avatar").attr("src", res.photo);
        }
        $(".action-block").empty();
        $("#list-item-sidebar").empty();
        
        var vUser = `<p class="desc">`+ res.username +`</p>
        <hr>
        <a href="./profile.html" class="login-title">Profile</a>
        <hr>
        <a href = "#!" class="real-estate-manage login-title">Your real estates</a>
        <hr>
        <a class="btn-out signout-title">Sign-out</a>`

        var vSidebarItem = `         
            <li><p class="desc">`+ res.username +`</p></li>
            <a href="./profile.html" class="profile-info">Profile</a>
            <a href = "#!" class="real-estate-manage">Your real estates</a>
            <li><a href="./real-estate-add.html" class="btn-add-property">Add real estate</a></li>
            <hr>
            <li><a href="#">Home</a></li>
            <li><a href="#footer">About</a></li>
            <li><a class="btn-out signout-sidebar">Sign out</a></li>`

        $("#list-item-sidebar").html(vSidebarItem);
        $(".action-block").html(vUser);
        $("#btn-add-property").removeClass("d-none");

        if (res.roles[0].name == "ROLE_CUSTOMER"){
            $(".real-estate-manage").attr("href", "./customer-manage.html");
        }
        if (res.roles[0].name == "ROLE_HOME_SELLER"){
            $(".real-estate-manage").attr("href", "./home-seller-manage.html");
        }
        if (res.roles[0].name == "ROLE_ADMIN"){
            $(".real-estate-manage").attr("href", "./admin-manage.html");
        }
    }
})