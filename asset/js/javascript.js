$(document).ready(function(){
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const vtoken = getCookie("token");
    var gRealEsateUrl = "http://localhost:8080/realestate/api/test/real_estates/posted";

    var gEmployeeUrL = "http://localhost:8080/realestate/api/test/employees?page=0&size=3";

    var gProvinceUrl = "http://localhost:8080/realestate/api/test/provinces";

    var gCustomerUrL = "http://localhost:8080/realestate/api/test/customers";

    var urlInfo = "http://localhost:8080/realestate/api/test/me";

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    $(document).on("click", "#btn-search", function(){
        onBtnSearchClick();
    })

    $("#btn-add-customer").on("click", function(){
        onBtnAddCustomerClick();
    })

    //Set sự kiện cho nút logout
    $(document).on("click", ".btn-out", function(){
        console.log("abc");
        redirectToLogin();
    });

    setInterval(onIconCarousel1Click, 3000);
    setInterval(onIconCarousel2Click, 6000);
    setInterval(onIconCarousel3Click, 9000);

    $("#carousel-1").on("click", function(){
        onIconCarousel1Click();
    })
    $("#carousel-2").on("click", function(){
        onIconCarousel2Click();
    })
    $("#carousel-3").on("click", function(){
        onIconCarousel3Click();
    })
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onIconCarousel1Click(){
        $(".carousel-item").prop("style", "transform: translateX(0%);");
        $("#carousel-1").addClass("active");
        $("#carousel-2").removeClass("active");
        $("#carousel-3").removeClass("active");
    }
    function onIconCarousel2Click(){
        $(".carousel-item").prop("style", "transform: translateX(-100%);")
        $("#carousel-2").addClass("active");
        $("#carousel-1").removeClass("active");
        $("#carousel-3").removeClass("active");
    }
    function onIconCarousel3Click(){
        $(".carousel-item").prop("style", "transform: translateX(-200%);")
        $("#carousel-3").addClass("active");
        $("#carousel-2").removeClass("active");
        $("#carousel-1").removeClass("active");
    }
    function onPageLoading(){
        "use strict"
        callApiCheckUser();
        callApiGetRealEstate();
        // callApiGetEmployee();
        callApiGetProvince();
    }

    // Hàm xử lí nhấn nút add customer
    function onBtnAddCustomerClick(){
        "use strict"
        var vCustomerInfo = {
            contactName: "",
            email: "",
            mobile: "",
        }

        // Read data customer form 
        readDataCustomerForm(vCustomerInfo);
        // Gọi api thêm customer vào database
        callApiAddCustomer(vCustomerInfo);

    }
    // Hàm xử lí sự kiện nhấn nút tìm kiếm
    function onBtnSearchClick(){
        "use strict"
        var vSearchObj = {
            request: "",
            provinceId: "",
            price1: "",
            price2: "",
            acreage1: "",
            acreage2: ""
        }
        // read data
        readDataSearch(vSearchObj);
        // validate data
        validateDataSearch(vSearchObj);

        callApiSearch(vSearchObj);
    };

    // Hàm kiểm tra người dùng đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                console.log(responseObject);
                responseHandler(responseObject);
            },
            error: function(xhr) {
                console.log(xhr);
            }
        });
    }

    // Hàm gọi api lấy real estate
    function callApiGetRealEstate(){
        "use strict"
        $.ajax({
            url: gRealEsateUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res){
                console.log(res);
                loadRealEstateToCard(res);
            },
            error: function(er){
                console.log(er.responseText);
            }
        })
    }
    // Hàm gọi api lấy employee
    function callApiGetEmployee(){
        "use strict"
        $.ajax({
            url: gEmployeeUrL,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res){
                console.log(res);
                loadEmployeeToCard(res);
            },
            error: function(er){
                console.log(er.responseText);
            }
        })
    }

    // Hàm gọi api add customer
    function callApiAddCustomer(paramObj){
        "use strict"
        var vJsonCustomerObj = JSON.stringify(paramObj);
        $.ajax({
            url: gCustomerUrL,
            type: "POST",
            contentType: "application/json",
            data: vJsonCustomerObj,
            success: function(res){
                console.log(res);
                alert("SUCCESS");
            },
            error: function(er){
                console.log(er.responseText);
            }
        })
    }

    // Hàm gọi api lấy province
    function callApiGetProvince(){
        "use strict"
        $.ajax({
            url: gProvinceUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res){
                console.log(res);
                loadProvinceToSelect(res);
            },
            error: function(er){
                console.log(er.responseText);
            }
        })
    }
    // Hàm gọi api SEARCH
    function callApiSearch(paramObj){
        "use strict"
        $.ajax({
            url: "http://localhost:8080/realestate/api/test/real_estates" + "/find/" 
                + "request/" + paramObj.request + "/province/" + paramObj.provinceId 
                + "/price1/"+ paramObj.price1 +"/price2/" + paramObj.price2 + "/acreage1/"
                + paramObj.acreage1 +"/acreage2/" + paramObj.acreage2,
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res){
                console.log(res);
                loadSearchResultoCard(res);
            },
            error: function(er){
                console.log(er.responseText);
            }
        })
    }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm thu thâp dữ liệu
    function readDataSearch(paramObj){
        "use strict"
        paramObj.request = $("#select-request").val();
        paramObj.provinceId = $("#select-province").val();
        paramObj.price1 = $("#select-price").val();
        paramObj.acreage1 = $("#select-acreage").val();
    }

     // Hàm thu thập dữ liệu
     function readDataCustomerForm(paramObj){
        "use strict"
        paramObj.contactName = $("#inp-name").val();
        paramObj.email = $("#inp-email").val();
        paramObj.mobile = $("#inp-mobile").val();
        paramObj.address = $("#inp-address").val();
        paramObj.contactTitle = $("#inp-title").val();
        paramObj.note = $("#inp-note").val();
    }

    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    // Hàm xử lí sau khi check đăng nhập user 
    function responseHandler(res){
        "use strict"
        if(res.photo != null){
            $("#user-avatar").attr("src", res.photo);
        }
        $("#list-item-sidebar").empty();
        $(".action-block").empty();
        var vUser = `<p class="desc">`+ res.username +`</p>
            <hr>
            <a href="./profile.html" class="login-title">Profile</a>
            <hr>
            <a href = "#!" class="login-title real-estate-manage">Your real estates</a>
            <hr>
            <a class="btn-out signout-title">Sign-out</a>`

        var vSidebarItem = `         
            <li><p class="desc">`+ res.username +`</p></li>
            <a href="./profile.html" class="profile-info">Profile</a>
            <a href = "#!" class="real-estate-manage">Your real estates</a>
            <li><a href="./real-estate-add.html" class="btn-add-property">Add real estate</a></li>
            <hr>
            <li><a href="#">Home</a></li>
            <li><a href="#properties">Properties</a></li>
            <li><a href="#team">Team</a></li>
            <li><a href="#contact">Contact</a></li>
            <li><a class="btn-out signout-sidebar">Sign out</a></li>`

        $(".action-block").html(vUser);
        $("#list-item-sidebar").html(vSidebarItem);
        $(".btn-add-property").removeClass("d-none");

        if (res.roles[0].name == "ROLE_CUSTOMER"){
            $(".real-estate-manage").attr("href", "./customer-manage.html");
        }
        if (res.roles[0].name == "ROLE_HOME_SELLER"){
            $(".real-estate-manage").attr("href", "./home-seller-manage.html");
        }
        if (res.roles[0].name == "ROLE_ADMIN"){
            $(".real-estate-manage").attr("href", "./admin-manage.html");
        }
    }
    // Hàm xử lí dữ liệu
    function validateDataSearch(paramObj){
        "use strict"
        if (paramObj.price1 == 0){
            paramObj.price1 = 0;
            paramObj.price2 = 999999999999;
        }
        if (paramObj.price1 == "10000000"){
            paramObj.price1 = 0;
            paramObj.price2 = 10000000;
        }
        if (paramObj.price1 == "15000000"){
            paramObj.price1 = 0;
            paramObj.price2 = 15000000;
        }
        if (paramObj.price1 == "20000000"){
            paramObj.price1 = 0;
            paramObj.price2 = 20000000;
        }
        if (paramObj.price1 == "25000000+"){
            paramObj.price1 = 25000000;
            paramObj.price2 = 999999999999;
        }
        if (paramObj.acreage1 == "0"){
            paramObj.acreage1 = 0;
            paramObj.acreage2 = 999999999999;
        }
        if (paramObj.acreage1 == "500"){
            paramObj.acreage1 = 0;
            paramObj.acreage2 = 500;
        }
        if (paramObj.acreage1 == "1000"){
            paramObj.acreage1 = 0;
            paramObj.acreage2 = 1000;
        }
        if (paramObj.acreage1 == "2000"){
            paramObj.acreage1 = 0;
            paramObj.acreage2 = 2000;
        }
        if (paramObj.acreage1 == "2000+"){
            paramObj.acreage1 = 2000;
            paramObj.acreage2 = 999999999999;
        }
    }
    //Load province to select box
    function loadProvinceToSelect(paramRes){
        "use strict"
        var vSelectBox = $("#select-province");

            for(var bI = 0; bI < paramRes.length; bI ++){
                    var bOption = $("<option>");
                    bOption.prop("value", paramRes[bI].id);
                    bOption.prop("text", paramRes[bI].name);
                    vSelectBox.append(bOption);
            }
    }
    //Load real estate to card
    function loadRealEstateToCard(paramArr){
        "use strict"
        var vListRealestate = $("#list-realestate");
        vListRealestate.empty();
        for (var bIndex = 0; bIndex < 3 ; bIndex ++){
            var bRealestateIte = `<div class="item">
            <a target = "_blank" href="./details.html?id=` + paramArr[bIndex].id + `"
                ><img
                    src="` + paramArr[bIndex].photo +`"
                    alt="property-1"
                    class="popular-img"
            /></a>
            <div class="info">
                <a target = "_blank" href="./details.html?id=` + paramArr[bIndex].id + `">
                    <h3 class="property-title">
                    ` + paramArr[bIndex].title +`
                    </h3>
                </a>
                <p class="address">
                    `+ paramArr[bIndex].street.name +`, `+ paramArr[bIndex].ward.name +`, `+ paramArr[bIndex].district.name +`, `+ paramArr[bIndex].province.name +`
                </p>
                <p class="date">Added: ` + paramArr[bIndex].date_create +`</p>
                <div class="utility">
                    <div class="utility-item">
                        <img
                            src="./asset/icons/sqft.JPG"
                            alt="SQFT"
                            class="icon"
                        /><p class="value">`+ paramArr[bIndex].acreage +` m<span class = "square-meter">2</span></p>
                    </div>
                    <div class="utility-item">
                        <img
                            src="./asset/icons/bed.JPG"
                            alt="Bed"
                            class="icon"
                        /><span class="value">` + paramArr[bIndex].bedroom +`</span>
                    </div>
                    <div class="utility-item">
                        <img
                            src="./asset/icons/bath.JPG"
                            alt="Bath"
                            class="icon"
                        /><span class="value">`+ paramArr[bIndex].bath +`</span>
                    </div>
                    <div class="utility-item">
                        <img
                            src="./asset/icons/garage.JPG"
                            alt="Garage"
                            class="icon"
                        /><span class="value">` + paramArr[bIndex].number_floors +`</span>
                    </div>
                </div>
                <hr />
                <p class="price">Price: ` + paramArr[bIndex].priceFormat + ` VND</p>
            </div>
        </div>`
        vListRealestate.append(bRealestateIte);
        }
    }
    //Load real search to card
    function loadSearchResultoCard(paramArr){
        "use strict"
        $("#search").prop("style", "display: block;");
        var vListRealestate = $("#list-result");
        vListRealestate.empty();
        if (paramArr.length > 0){
            $("#title-result").html("Real Estate Found")
            for (var bIndex = 0; bIndex < 6 ; bIndex ++){
                var bRealestateIte = `<div class="item">
                <a target = "_blank" class="a-img" href="./details.html?id=` + paramArr[bIndex].id + `"
                    ><img
                        src="` + paramArr[bIndex].photo +`"
                        alt="property-1"
                        class="search-result-img"
                /></a>
                <div class="info">
                    <a target = "_blank" href="./details.html?id=` + paramArr[bIndex].id + `">
                        <h3 class="property-title">
                        ` + paramArr[bIndex].title +`
                        </h3>
                    </a>
                    <p class="address">
                        `+ paramArr[bIndex].street.name +`, `+ paramArr[bIndex].ward.name +`, `+ paramArr[bIndex].district.name +`, `+ paramArr[bIndex].province.name +`
                    </p>
                    <p class="date">Added: ` + paramArr[bIndex].date_create +`</p>
                    <div class="utility">
                        <div class="utility-item">
                            <img
                                src="./asset/icons/sqft.JPG"
                                alt="SQFT"
                                class="icon"
                            /><p class="value">`+ paramArr[bIndex].acreage +` m<span class = "square-meter">2</span></p>
                        </div>
                        <div class="utility-item">
                            <img
                                src="./asset/icons/bed.JPG"
                                alt="Bed"
                                class="icon"
                            /><span class="value">` + paramArr[bIndex].bedroom +`</span>
                        </div>
                        <div class="utility-item">
                            <img
                                src="./asset/icons/bath.JPG"
                                alt="Bath"
                                class="icon"
                            /><span class="value">`+ paramArr[bIndex].bath +`</span>
                        </div>
                        <div class="utility-item">
                            <img
                                src="./asset/icons/garage.JPG"
                                alt="Garage"
                                class="icon"
                            /><span class="value">` + paramArr[bIndex].number_floors +`</span>
                        </div>
                    </div>
                    <hr />
                    <p class="price">Price: ` + paramArr[bIndex].priceFormat + ` VND</p>
                </div>
            </div>`
            vListRealestate.append(bRealestateIte);
            }
        }else {
            $("#title-result").html("No results!")
        }
    }
    //Load employee to card
    function loadEmployeeToCard(paramArr){
        "use strict"
        var vListMember = $("#member-list");
        vListMember.empty();
        for (var bIndex = 0; bIndex < 3 ; bIndex ++){
            var bEmployeeItem = `<div class="member">
            <img
                src="` + paramArr[bIndex].photo + `"
                alt="` + paramArr[bIndex].title + `"
                class="avatar"
            />
            <div class="status">
                <p class="name">` + paramArr[bIndex].firstName + " " + paramArr[bIndex].lastName +`</p>
                <p class="desc">` + paramArr[bIndex].title + `</p>
            </div>
        </div>`
        vListMember.append(bEmployeeItem);
        }
    }
    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
})