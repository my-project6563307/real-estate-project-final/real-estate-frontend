$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const vtoken = getCookie("token");
    var gInitialDataTable = {
            paging: true,
            lengthChange: false,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: false,
            columns: "",
            columnDefs: "",
    }
    var gRealEsateUrl = "http://localhost:8080/realestate/real_estates/current-employee";

    var gSavedDataTable = "";

    var gRowSelectedId = "";

    var urlInfo = "http://localhost:8080/realestate/api/test/me";

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    $("#btn-logout").on("click", function() {
        redirectToLogin();
    });
    // Khởi tạo sự kiện nhấn icon edit trên bảng
    $(document).on("click", "#edit-real-estate", function(){
        onRowClick(this);
        window.location.href = "./subpage/real-estate-seller-edit.html?id=" + gRowSelectedId;
    })

    // Thêm sự kiện nhất nút thêm dữ liệu
    $(document).on("click", "#btn-real-estate", function(){
        window.location.href = "./subpage/real-estate-seller-customer-add.html";
    })

    // Thêm sự kiện nhấn icon delete
    $(document).on("click", "#delete-real-estate", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-realestate")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })

    // Thêm sự kiện nhấn nút xác nhận xoá dữ liệu

    $(document).on("click", "#btn-delete-realestate", function(){
        callApiDeleteRowById("real_estates");
    })
    
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
            callApiCheckUser();

            var vRealestateTable = createRealEstateDataTable();
        
            gSavedDataTable = vRealestateTable;

            callApiGetRealEstates(vRealestateTable);
    }
    // Hàm check user đăng nhập
    function callApiCheckUser(){
        $.ajax({
            url: urlInfo,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function(responseObject) {
                console.log(responseObject);
                responseHandler(responseObject);
            },
            error: function(xhr) {
                console.log(xhr);
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                // redirectToLogin()
            }
        });
    }

    // Hàm gọi Api xoá row
    function callApiDeleteRowById(paramTableName){
        $.ajax({
            url: "http://localhost:8080/realestate/"+ paramTableName + "/" + gRowSelectedId,
            type: "DELETE",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function (res) {
                console.log(res);
                alert("delete successfully!");
                window.location.href = window.location.href;
                $("#" + paramTableName + "").click();
            },
            error: function (err) {
                console.log(err.responseText);
                $("#delete-modal").modal("hide");
                alert("Fail to delete data!");
            }
        })
    }

    //Hàm gọi api lấy Real estate
    function callApiGetRealEstates(paramTable) {
        "use strict";
        $.ajax({
            url: gRealEsateUrl,
            type: "GET",
            dataType: "json",
            headers: {
                "Authorization": "Bearer " + vtoken + "" 
              },
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Real estate!");
            },
        });
    }
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "");
        window.location.href = "./login.html";
    }

    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }

    // Hàm xử lí khi click vào row table
    function onRowClick(paramIcon){
        "use trict"
        var vRow = $(paramIcon).closest("tr");
        gRowSelectedId = "";
        var vDataOnRow = gSavedDataTable.row(vRow).data();

        gDataSelectedRow = vDataOnRow;
        gRowSelectedId = vDataOnRow.id;
        console.log(gRowSelectedId);
    }

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    // Hàm xử lí sau khi check đăng nhập user 
    function responseHandler(res){
        "use strict"
        $("#user-account-name").html(res.username);
    }

    //Load dữ liệu vào bảng
    function loadDataToTable(paramArr, paramTable) {
        "use strict";
        paramTable.clear();
        paramTable.rows.add(paramArr);
        paramTable.draw();
    }

    // Tạo data table bảng Real estate
    function createRealEstateDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "accepted" },
            { data: "type" },
            { data: "request" },
            { data: "direction" },
            { data: "furniture_type" },
            { data: "photo" },
            { data: "address" },
            { data: "acreage" },
            { data: "province.name" },
            { data: "district.name" },
            { data: "ward.name" },
            { data: "price" },
            { data: "date_create" },
            { data: "bedroom" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-real-estate" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-real-estate" class="fa fa-solid fa-trash-can delete-icon"></i>',
            },
            {   
                targets: 2,
                render: function(data){
                    if(data == true){
                        return `<i class="fa-solid fa-circle-check text-success"></i>`
                    }
                    if(data == false){
                        return `<i class="fa-solid fa-square-xmark text-danger"></i>`
                    }
                }
            },
            {   
                targets: 3,
                render: function(data){
                    if(data == 0){
                        return "Đất"
                    }
                    if(data == 1){
                        return "Nhà ở"
                    }
                    if(data == 2){
                        return "Căn hộ/ chung cư"
                    }
                    if(data == 3){
                        return "Văn phòng/ mặt bằng"
                    }
                    if(data == 4){
                        return "Kinh doanh"
                    }
                    if(data == 5){
                        return "Phòng trọ"
                    }
                    
                }
            },
            {   
                targets: 4,
                render: function(data){
                    if(data == 0){
                        return "Cần bán"
                    }
                    if(data == 1){
                        return "Cần mua"
                    }
                    if(data == 2){
                        return "Cho thuê"
                    }
                    if(data == 3){
                        return "Cần thuê"
                    }
                }
            },
            {   
                targets: 5,
                render: function(data){
                    if(data == 0){
                        return "Tây"
                    }
                    if(data == 1){
                        return "Bắc"
                    }
                    if(data == 2){
                        return "Đông"
                    }
                    if(data == 3){
                        return "Nam"
                    }
                    if(data == 4){
                        return "Tây Bắc"
                    }
                    if(data == 5){
                        return "Tây Nam"
                    }
                    if(data == 6){
                        return "Đông Bắc"
                    }
                    if(data == 7){
                        return "Đông Nam"
                    }
                    if(data == 8){
                        return "Chưa xác định"
                    }
                }
            },
            {   
                targets: 6,
                render: function(data){
                    if(data == 0){
                        return "Không có"
                    }
                    if(data == 1){
                        return "Cơ bản"
                    }
                    if(data == 2){
                        return "Đầy đủ"
                    }
                    if(data == 3){
                        return "Chưa biết"
                    }
                }
            },
            {   
                targets: 7,
                render: function(data){
                    return `
                    <img src="` + data + `" alt="photo" style="width: 150px; height: 150px;object-fit:cover;">`
                }
            }
          ];
        var vTable = $("#table-real-estate").DataTable(gInitialDataTable);
        return vTable;
    }
    onPageLoading();
});