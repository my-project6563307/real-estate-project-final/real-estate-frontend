$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");

        var gProvinceUrl = "http://localhost:8080/realestate/api/test/provinces"
        var gDistrictUrl = "http://localhost:8080/realestate/districts"
        var gWardUrl = "http://localhost:8080/realestate/wards"
        var gStreetUrl = "http://localhost:8080/realestate/streets"
        var gProjectUrl = "http://localhost:8080/realestate/projects"
        var gRealEsateUrl = "http://localhost:8080/realestate/real_estates";

        var urlInfo = "http://localhost:8080/realestate/api/test/me";
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();

        //Set sự kiện cho nút logout
        $(document).on("click", ".btn-out", function(){
            console.log("abc");
            redirectToLogin();
        });
        // Thêm sự kiện nhất nút thêm dữ liệu
        $(document).on("click", "#btn-add", function(){
            onBtnAddClick();
        })
        // Xử lí lấy ảnh
        $("#inp-photo").change(function(e) {
            $("#upload-photo").empty();
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
        
                var file = e.originalEvent.srcElement.files[i];
        
                var img = document.createElement("img");
                img.setAttribute("id", "photo-upload");
                var reader = new FileReader();
                reader.onloadend = function() {
                     img.src = reader.result;
                }
                reader.readAsDataURL(file);
                $("#upload-photo").html(img);
            }
        });
        // Tạo sự kiện nhấn nút thay đổi select
        $(document).on("change", "#select-province", function(){
            var vNewProvinceId = $("#select-province :selected").val();
            $("#select-district").empty();
            $("#select-ward").empty();
            $("#select-street").empty();
            $("#select-ward").attr("disabled", true);
            $("#select-street").attr("disabled", true);
            callApiGetDistrictByProvinceId(vNewProvinceId);
        })
    
        $(document).on("change", "#select-district", function(){
            var nNewDistrictId = $("#select-district :selected").val();
            console.log(nNewDistrictId);
            $("#select-ward").empty();
            $("#select-street").empty();
            $("#select-ward").attr("disabled", false);
            $("#select-street").attr("disabled", false);
            callApiGetWardByDistrictId(nNewDistrictId);
            callApiGetStreetByDistrictId(nNewDistrictId);
        })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiCheckUser();
            callApiGetProvince();
            callApiGetProject()
        }
        
        //Hàm xử lí sự kiện nhất nút cập nhật
        function onBtnAddClick(){
            "use strict"
            // khai báo dữ liệu cập nhật
            var vDataUpdate = {
                type: "",
                request: "",
                address: "",
                title: "",
                price: "",
                priceMin: "",
                acreage: "",
                direction: "",
                total_floors: "",
                number_floors: "",
                bath: "",
                apart_code: "",
                wall_area: "",
                bedroom: "",
                balcony: "",
                landscape_view: "",
                apart_loca: "",
                apart_type: "",
                furniture_type: "",
                price_rent: "",
                legal_doc: "",
                description: "",
                width_y: "",
                long_x: "",
                street_house: "",
                lat: "",
                lng: "",
                provinceId: "",
                districtId: "",
                wardId: "",
                streetId: "",
                projectId: "",
                photo: ""
            }
            //B1: Thu thập dữ liệu
            readDataForm(vDataUpdate);
            console.log(vDataUpdate);
            //B2: Kiểm tra dữ liệu(bỏ qua)
            //B3: Cập nhật dữ liệu
            callApiAddRealEstate(vDataUpdate);
    
        }

        // Hàm gọi api cập nhật project map by id
        function callApiAddRealEstate(paramObj) {
            "use strict";
            var vJsonUpdateData = JSON.stringify(paramObj);
            $.ajax({
                url: gRealEsateUrl 
                + "?projectId=" + paramObj.projectId
                + "&customerId=" + paramObj.customerId
                + "&provinceId=" + paramObj.provinceId
                + "&districtId=" + paramObj.districtId
                + "&wardId=" + paramObj.wardId
                + "&streetId=" + paramObj.streetId,
                type: "POST",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                data: vJsonUpdateData,
                success: function (res) {
                    console.log(res);
                    alert("successfully!")
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to update!");
                },
            });
        }

        // Hàm kiểm tra người dùng đăng nhập
        function callApiCheckUser(){
            $.ajax({
                url: urlInfo,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                },
                success: function(responseObject) {
                    console.log(responseObject);
                    responseHandler(responseObject);
                },
                error: function(xhr) {
                    console.log(xhr);
                }
            });
        }
        // Hàm gọi lấy tất cả province
        function callApiGetProvince(){
            "use strict";
            $.ajax({
                url: gProvinceUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "province");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả project
        function callApiGetProject(){
            "use strict";
            $.ajax({
                url: gProjectUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "project");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }

        // Hàm gọi lấy tất cả district by province id
        function callApiGetDistrictByProvinceId(paramId){
            "use strict";
            $.ajax({
                url: gDistrictUrl + "/province/" + paramId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "district");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy ward by district id
        function callApiGetWardByDistrictId(paramId){
            "use strict";
            $.ajax({
                url: gWardUrl + "/district/" + paramId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "ward");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả street by district id
        function callApiGetStreetByDistrictId(paramId){
            "use strict";
            $.ajax({
                url: gStreetUrl + "/district/" + paramId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "street");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }
    //Hàm setCookie
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    // Hàm xử lí sau khi check đăng nhập user 
    function responseHandler(res){
        "use strict"
        if(res.photo != null){
            $("#user-avatar").attr("src", res.photo);
        }

        $("#list-item-sidebar").empty();
        $(".action-block").empty();
        var vUser = `<p class="desc">`+ res.username +`</p>
        <hr>
        <a href="./profile.html" class="login-title">Profile</a>
        <hr>
        <a href = "#!" class="real-estate-manage login-title">Your real estates</a>
        <hr>
        <a class="btn-out signout-title">Sign-out</a>`

        var vSidebarItem = `         
            <li><p class="desc">`+ res.username +`</p></li>
            <a href="./profile.html" class="profile-info">Profile</a>
            <a href = "#!" class="real-estate-manage">Your real estates</a>
            <hr>
            <li><a href="#">Home</a></li>
            <li><a href="#footer">About</a></li>
            <li><a class="btn-out signout-sidebar">Sign out</a></li>`

        $(".action-block").html(vUser);
        $("#list-item-sidebar").html(vSidebarItem);

        if (res.roles[0].name == "ROLE_CUSTOMER"){
            $(".real-estate-manage").attr("href", "./customer-manage.html");
        }
        if (res.roles[0].name == "ROLE_HOME_SELLER"){
            $(".real-estate-manage").attr("href", "./home-seller-manage.html");
        }
        if (res.roles[0].name == "ROLE_ADMIN"){
            $(".real-estate-manage").attr("href", "./admin-manage.html");
        }
    }

    // Hàm tải dữ liệu lên form
    function loadDataToSelectBox(paramArr, paramName){
        "use strict"
        var vSelectBox = $("#select-"+ paramName +"");

        for(var bI = 0; bI < paramArr.length; bI ++){
                var bOption = $("<option>");
                bOption.prop("value", paramArr[bI].id);
                bOption.prop("text", paramArr[bI].name);
                vSelectBox.append(bOption);
        }
    }

    // Hàm thu thập dữ liệu
    function readDataForm(paramObj){
        "use strict"
        paramObj.type = $("#select-type").val();
        paramObj.request = $("#select-request").val();
        paramObj.address = $("#inp-address").val();
        paramObj.title = $("#inp-title").val();
        paramObj.price = $("#inp-price").val();
        paramObj.acreage = $("#inp-acreage").val();
        paramObj.number_floors = $("#inp-number-floors").val();
        paramObj.bath = $("#inp-bath").val();
        paramObj.apart_code = $("#inp-apart-code").val();
        paramObj.bedroom = $("#inp-bedroom").val();
        paramObj.balcony = $("#select-balcony").val();
        paramObj.furniture_type = $("#select-furniture-type").val();
        paramObj.description = $("#inp-description").val();
        paramObj.price_rent = $("#inp-price-rent").val();
        paramObj.photo = $("#photo-upload").attr("src");

        paramObj.provinceId = $("#select-province :selected" ).val();
        paramObj.districtId = $("#select-district :selected" ).val();
        paramObj.wardId = $("#select-ward :selected" ).val();
        paramObj.streetId = $("#select-street :selected" ).val();
        paramObj.projectId = $("#select-project :selected" ).val();
        paramObj.direction = $("#select-direction :selected" ).val();
    }

})