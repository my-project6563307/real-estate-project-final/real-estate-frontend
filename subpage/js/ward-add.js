$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");

        var gUrl = "http://localhost:8080/realestate/wards";
        var gProvinceUrl = "http://localhost:8080/realestate/api/test/provinces";
        var gDistrictUrl = "http://localhost:8080/realestate/districts";
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();
    
        $(document).on("click", "#btn-add", function(){
            onBtnAddClick();
        })
        $(document).on("change", "#select-province", function(){
            onChangeProvinceSelectBox();
        })
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiGetAllProvince();
        }
    
        // Hàm xử lí change province
        function onChangeProvinceSelectBox(){
            "use strict"
            var vId = $("#select-province :selected").val();
            $("#select-district").empty();
            callApiGetDistrictByProvinceId(vId);
        }
        //Hàm xử lí sự kiện nhất nút cập nhật
        function onBtnAddClick(){
            "use strict"
            // khai báo dữ liệu cập nhật
            var vDataUpdate = {
                name:"",
                prefix: "",
                provinceId: "",
                districtId: "",
            }
            //B1: Thu thập dữ liệu
            readDataForm(vDataUpdate);
            console.log(vDataUpdate);
            //B2: Kiểm tra dữ liệu(bỏ qua)
            //B3: Cập nhật dữ liệu
            callApiAddWard(vDataUpdate);
    
        }
        // Hàm gọi api lấy province
        function callApiGetAllProvince() {
            "use strict";
            $.ajax({
                url: gProvinceUrl ,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelect(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi api lấy district by province id
        function callApiGetDistrictByProvinceId(paramId){
            "use strict";
            $.ajax({
                url: gDistrictUrl + "/province/" + paramId ,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDistrictToSelect(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi api add ward
        function callApiAddWard(paramObj) {
            "use strict";
            var vJsonUpdateData = JSON.stringify(paramObj);
            $.ajax({
                url: gUrl + "/province/" + paramObj.provinceId
                + "/district/" + paramObj.districtId,
                type: "POST",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                data: vJsonUpdateData,
                success: function (res) {
                    console.log(res);
                    alert("successfully!")
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to create!");
                },
            });
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // Hàm thu thập dữ liệu
        function readDataForm(paramObj){
            "use strict"
            paramObj.name = $("#inp-name").val();

            if ($("#select-prefix :selected").val() == 0){
                paramObj.prefix = "Phường";
            }
            else if ($("#select-prefix :selected").val() == 1) {
                
                paramObj.prefix = "Xã";
            }
            else if ($("#select-prefix :selected").val() == 2) {

                paramObj.prefix = "Thị trấn";
            }
            paramObj.provinceId = $("#select-province :selected").val();
            paramObj.districtId = $("#select-district :selected").val();
        }

        // Hàm load dữ liệu vào ô select
        function loadDataToSelect(paramArr){
            "use strict"
            var vSelectBox = $("#select-province");

            for(var bI = 0; bI < paramArr.length; bI ++){
                    var bOption = $("<option>");
                    bOption.prop("value", paramArr[bI].id);
                    bOption.prop("text", paramArr[bI].name);
                    vSelectBox.append(bOption);
            }
        }
        // Hàm load dữ liệu vào ô select
        function loadDistrictToSelect(paramArr){
            "use strict"
            var vSelectBox = $("#select-district");

            for(var bI = 0; bI < paramArr.length; bI ++){
                    var bOption = $("<option>");
                    bOption.prop("value", paramArr[bI].id);
                    bOption.prop("text", paramArr[bI].name);
                    vSelectBox.append(bOption);
            }
        }

        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    });