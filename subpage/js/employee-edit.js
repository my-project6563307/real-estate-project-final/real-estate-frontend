$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");
        var gUrl = new URL(window.location.href);
    
        var gId = gUrl.searchParams.get("id");
    
        var gUrl = "http://localhost:8080/realestate/employees";
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();
    
        $(document).on("click", "#btn-update", function(){
            onBtnUpdateClick();
        })

        $("#inp-photo").change(function(e) {
            $("#photo-upload").attr("src","");
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
        
                var file = e.originalEvent.srcElement.files[i];
        
                var img = $("#photo-upload");

                var reader = new FileReader();
                    reader.onloadend = function() {
                    img.attr("src", reader.result);
                }
                reader.readAsDataURL(file);
                $("#upload-photo").html(img);
            }
        });
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiGetEmployeesById(gId);
        }
    
        //Hàm xử lí sự kiện nhất nút cập nhật
        function onBtnUpdateClick(){
            "use strict"
            // khai báo dữ liệu cập nhật
            var vDataUpdate = {
                firstName:"",
                lastName: 0,
                address: "",
                birthDate: "",
                country: "",
                city: "",
                homePhone: "",
                username: "",
                email: "",
                extension: "",
                hireDate: "",
                postalCode: "",
                photo: "",
                profile: "",
                region: "",
                notes: "",
                title: "",
                titleOfCourtesy: "",
                userLevel: "",
                roleId: ""
            }
            //B1: Thu thập dữ liệu
            readDataForm(vDataUpdate);
            console.log(vDataUpdate);
            //B2: Kiểm tra dữ liệu(bỏ qua)
            //B3: Cập nhật dữ liệu
            callApiUpdateEmployee(vDataUpdate);
    
        }
        // Hàm gọi api lấy address map by id
        function callApiGetEmployeesById(paramId) {
            "use strict";
            $.ajax({
                url: gUrl + "/" + paramId ,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToForm(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get employee!");
                },
            });
        }
        // Hàm gọi api cập nhật address map by id
        function callApiUpdateEmployee(paramObj) {
            "use strict";
            var vJsonUpdateData = JSON.stringify(paramObj);
            $.ajax({
                url: gUrl + "/" + gId + "/role/" +paramObj.roleId,
                type: "PUT",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                data: vJsonUpdateData,
                success: function (res) {
                    console.log(res);
                    alert("successfully!")
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to update!");
                },
            });
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // Hàm tải dữ liệu vào form
        function loadDataToForm(paramObj){
            "use strict"
            $("#inp-id").val(paramObj.id);
            $("#inp-first-name").val(paramObj.firstName);
            $("#inp-last-name").val(paramObj.lastName);
            $("#inp-address").val(paramObj.address);
            $("#inp-birth-date").val(paramObj.birthDate);
            $("#inp-country").val(paramObj.country);
            $("#inp-city").val(paramObj.city);
            $("#inp-home-phone").val(paramObj.homePhone);
            $("#inp-username").val(paramObj.username);
            $("#inp-email").val(paramObj.email);
            $("#inp-extension").val(paramObj.extension);
            $("#inp-hire-date").val(paramObj.hireDate);
            $("#inp-postal-code").val(paramObj.postalCode);
            $("#inp-profile").val(paramObj.profile);
            $("#inp-region").val(paramObj.region);
            $("#inp-notes").val(paramObj.notes);
            $("#inp-title").val(paramObj.title);
            $("#inp-title-of-courtesy").val(paramObj.titleOfCourtesy);
            $("#inp-user-level").val(paramObj.userLevel);
            $("#select-role").val(paramObj.roles[0].id);
            $("#photo-upload").attr("src", paramObj.photo);
        }
    
        // Hàm thu thập dữ liệu
        function readDataForm(paramObj){
            "use strict"
            paramObj.firstName = $("#inp-first-name").val();
            paramObj.lastName = $("#inp-last-name").val();
            paramObj.address = $("#inp-address").val();
            paramObj.birthDate = $("#inp-birth-date").val();
            paramObj.country = $("#inp-country").val();
            paramObj.city = $("#inp-city").val();
            paramObj.homePhone = $("#inp-home-phone").val();
            paramObj.username = $("#inp-username").val();
            paramObj.email = $("#inp-email").val();
            paramObj.extension = $("#inp-extension").val();
            paramObj.hireDate = $("#inp-hire-date").val();
            paramObj.postalCode = $("#inp-postal-code").val();
            paramObj.profile = $("#inp-profile").val();
            paramObj.region = $("#inp-region").val();
            paramObj.notes = $("#inp-notes").val();
            paramObj.title = $("#inp-title").val();
            paramObj.titleOfCourtesy = $("#inp-title-of-courtesy").val();
            paramObj.roleId = $("#select-role :selected").val();
            paramObj.photo = $("#photo-upload").attr("src");
        }

        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    });