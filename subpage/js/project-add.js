$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");
  
        var gUrl = "http://localhost:8080/realestate/projects";
        var gProvinceUrl = "http://localhost:8080/realestate/api/test/provinces"
        var gDistrictUrl = "http://localhost:8080/realestate/districts"
        var gWardUrl = "http://localhost:8080/realestate/wards"
        var gStreetUrl = "http://localhost:8080/realestate/streets"
        var gContractorUrl = "http://localhost:8080/realestate/contractors"
        var gDesignUnitUrl = "http://localhost:8080/realestate/design_units"
        var gInvestorUrl = "http://localhost:8080/realestate/investors"
        var gRegionLinkUrl = "http://localhost:8080/realestate/region_links"
        var gUtilitiesUrl = "http://localhost:8080/realestate/utilities"
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();

        $(document).on("click", "#btn-add", function(){
            onBtnAddClick();
        })

        $(document).on("change", "#select-province", function(){
            var vNewProvinceId = $("#select-province :selected").val();
            $("#select-district").empty();
            $("#select-ward").empty();
            $("#select-street").empty();
            $("#select-ward").attr("disabled", true);
            $("#select-street").attr("disabled", true);
            callApiGetDistrictByProvinceId(vNewProvinceId);
        })
    
        $(document).on("change", "#select-district", function(){
            var nNewDistrictId = $("#select-district :selected").val();
            console.log(nNewDistrictId);
            $("#select-ward").empty();
            $("#select-street").empty();
            $("#select-ward").attr("disabled", false);
            $("#select-street").attr("disabled", false);
            callApiGetWardByDistrictId(nNewDistrictId);
            callApiGetStreetByDistrictId(nNewDistrictId);
        })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiGetProvince();
            callApiGetContractor();
            callApiGetDesignUnit();
            callApiGetInvestor();
            callApiGetRegionLink();
            callApiGetUtilities();
        }
    
        //Hàm xử lí sự kiện nhất nút cập nhật
        function onBtnAddClick(){
            "use strict"
            // khai báo dữ liệu cập nhật
            var vDataUpdate = {
                name: "",
                acreage: 0,
                address: "",
                numApartment: "",
                numBlock: "",
                numFloors: "",
                description: "",
                slogan: "",
                constructArea: "",
                apartmenttArea: "",
                photo: "",
                lat: "",
                lng: "",
                provinceId: "",
                districtId: "",
                wardId: "",
                streetId: "",
                contractorId: "",
                designUnitId: "",
                investorId: "",
                regionLinkId: "",
                utilitiesId: ""
            }
            //B1: Thu thập dữ liệu
            readDataForm(vDataUpdate);
            console.log(vDataUpdate);
            //B2: Kiểm tra dữ liệu(bỏ qua)
            //B3: Cập nhật dữ liệu
            callApiAddProject(vDataUpdate);
    
        }

        // Hàm gọi api add project
        function callApiAddProject(paramObj) {
            "use strict";
            var vJsonUpdateData = JSON.stringify(paramObj);
            $.ajax({
                url: gUrl + "?constructionId=" + paramObj.contractorId 
                + "&designUnitId=" + paramObj.designUnitId
                + "&investorId=" + paramObj.investorId
                + "&regionLinkId=" + paramObj.regionLinkId
                + "&utilitiesId=" + paramObj.utilitiesId
                + "&provinceId=" + paramObj.provinceId
                + "&districtId=" + paramObj.districtId
                + "&wardId=" + paramObj.wardId
                + "&streetId=" + paramObj.streetId,
                type: "POST",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                data: vJsonUpdateData,
                success: function (res) {
                    console.log(res);
                    alert("successfully!")
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to update!");
                },
            });
        }

        // Hàm gọi lấy tất cả province
        function callApiGetProvince(){
            "use strict";
            $.ajax({
                url: gProvinceUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "province");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả district by province id
        function callApiGetDistrictByProvinceId(paramProvinceId){
            "use strict";
            $.ajax({
                url: gDistrictUrl + "/province/" + paramProvinceId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "district");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy ward by district id
        function callApiGetWardByDistrictId(paramDistrictId){
            "use strict";
            $.ajax({
                url: gWardUrl + "/district/" + paramDistrictId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "ward");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả street by district id
        function callApiGetStreetByDistrictId(paramDistrictId){
            "use strict";
            $.ajax({
                url: gStreetUrl + "/district/" + paramDistrictId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "street");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả contractor
        function callApiGetContractor(){
            "use strict";
            $.ajax({
                url: gContractorUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "contractor");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả design unit
        function callApiGetDesignUnit(){
            "use strict";
            $.ajax({
                url: gDesignUnitUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "design-unit");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả investor
        function callApiGetInvestor(){
            "use strict";
            $.ajax({
                url: gInvestorUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "investor");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả region link
        function callApiGetRegionLink(){
            "use strict";
            $.ajax({
                url: gRegionLinkUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "region-link");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả region link
        function callApiGetUtilities(){
            "use strict";
            $.ajax({
                url: gUtilitiesUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "utilities");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // Hàm thu thập dữ liệu
        function readDataForm(paramObj){
            "use strict"
            paramObj.name = $("#inp-name").val();
            paramObj.acreage = $("#inp-acreage").val();
            paramObj.address = $("#inp-address").val();
            paramObj.numApartment = $("#inp-num-apartment").val();
            paramObj.numBlock = $("#inp-num-block").val();
            paramObj.numFloors = $("#inp-num-floors").val();
            paramObj.description = $("#inp-description").val();
            paramObj.slogan = $("#inp-slogan").val();
            paramObj.constructArea = $("#inp-construct-area").val();
            paramObj.apartmenttArea = $("#inp-apartmentt-area").val();
            paramObj.photo = $("#inp-photo").val();
            paramObj.lat = $("#inp-lat").val();
            paramObj.lng = $("#inp-lng").val();

            paramObj.provinceId = $("#select-province :selected" ).val();
            paramObj.districtId = $("#select-district :selected" ).val();
            paramObj.wardId = $("#select-ward :selected" ).val();
            paramObj.streetId = $("#select-street :selected" ).val();
            paramObj.contractorId = $("#select-contractor :selected" ).val();
            paramObj.designUnitId = $("#select-design-unit :selected" ).val();
            paramObj.investorId = $("#select-investor :selected" ).val();
            paramObj.regionLinkId = $("#select-region-link :selected" ).val();
            paramObj.utilitiesId = $("#select-utilities :selected" ).val();
        }

        // Hàm tải dữ liệu province lên form
        function loadDataToSelectBox(paramArr, paramName){
            "use strict"
            var vSelectBox = $("#select-"+ paramName +"");

            for(var bI = 0; bI < paramArr.length; bI ++){
                    var bOption = $("<option>");
                    bOption.prop("value", paramArr[bI].id);
                    bOption.prop("text", paramArr[bI].name);
                    vSelectBox.append(bOption);
            }
        }

        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    });