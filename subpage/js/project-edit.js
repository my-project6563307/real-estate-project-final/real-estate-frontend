$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");
        var gUrl = new URL(window.location.href);
    
        var gId = gUrl.searchParams.get("id");

        var gUrl = "http://localhost:8080/realestate/projects";
        var gProvinceUrl = "http://localhost:8080/realestate/api/test/provinces"
        var gDistrictUrl = "http://localhost:8080/realestate/districts"
        var gWardUrl = "http://localhost:8080/realestate/wards"
        var gStreetUrl = "http://localhost:8080/realestate/streets"
        var gContractorUrl = "http://localhost:8080/realestate/contractors"
        var gDesignUnitUrl = "http://localhost:8080/realestate/design_units"
        var gInvestorUrl = "http://localhost:8080/realestate/investors"
        var gRegionLinkUrl = "http://localhost:8080/realestate/region_links"
        var gUtilitiesUrl = "http://localhost:8080/realestate/utilities"

        var gProjectObj = "";
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();

        $(document).on("click", "#btn-update", function(){
            onBtnUpdateClick();
        })

        $(document).on("change", "#select-province", function(){
            var vNewProvinceId = $("#select-province :selected").val();
            $("#select-district").empty();
            $("#select-ward").empty();
            $("#select-street").empty();
            $("#select-ward").attr("disabled", true);
            $("#select-street").attr("disabled", true);
            callApiGetDistrictByProvinceId(vNewProvinceId);
        })
    
        $(document).on("change", "#select-district", function(){
            var nNewDistrictId = $("#select-district :selected").val();
            console.log(nNewDistrictId);
            $("#select-ward").empty();
            $("#select-street").empty();
            $("#select-ward").attr("disabled", false);
            $("#select-street").attr("disabled", false);
            callApiGetWardByDistrictId(nNewDistrictId);
            callApiGetStreetByDistrictId(nNewDistrictId);
        })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiGetProjectById(gId);
        }
    
        //Hàm xử lí sự kiện nhất nút cập nhật
        function onBtnUpdateClick(){
            "use strict"
            // khai báo dữ liệu cập nhật
            var vDataUpdate = {
                name: "",
                acreage: 0,
                address: "",
                numApartment: "",
                numBlock: "",
                numFloors: "",
                description: "",
                slogan: "",
                constructArea: "",
                apartmenttArea: "",
                photo: "",
                lat: "",
                lng: "",
                provinceId: "",
                districtId: "",
                wardId: "",
                streetId: "",
                contractorId: "",
                designUnitId: "",
                investorId: "",
                regionLinkId: "",
                utilitiesId: ""
            }
            //B1: Thu thập dữ liệu
            readDataForm(vDataUpdate);
            console.log(vDataUpdate);
            //B2: Kiểm tra dữ liệu(bỏ qua)
            //B3: Cập nhật dữ liệu
            callApiUpdateProject(vDataUpdate);
    
        }
        // Hàm gọi api lấy address map by id
        function callApiGetProjectById(paramId) {
            "use strict";
            $.ajax({
                url: gUrl + "/" + paramId,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                async:false,
                success: function (res) {
                    console.log(res);
                    gProjectObj = res;
                    callApiGetProvince();
                    callApiGetDistrictByProvinceId(res.province.id);
                    callApiGetWardByDistrictId(res.district.id);
                    callApiGetStreetByDistrictId(res.district.id);
                    callApiGetContractor();
                    callApiGetDesignUnit();
                    callApiGetInvestor();
                    callApiGetRegionLink();
                    callApiGetUtilities();
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
            loadDataToForm(gProjectObj);
        }
        // Hàm gọi api cập nhật project map by id
        function callApiUpdateProject(paramObj) {
            "use strict";
            var vJsonUpdateData = JSON.stringify(paramObj);
            $.ajax({
                url: gUrl + "/" + gId + "?constructionId=" + paramObj.contractorId 
                + "&designUnitId=" + paramObj.designUnitId
                + "&investorId=" + paramObj.investorId
                + "&regionLinkId=" + paramObj.regionLinkId
                + "&utilitiesId=" + paramObj.utilitiesId
                + "&provinceId=" + paramObj.provinceId
                + "&districtId=" + paramObj.districtId
                + "&wardId=" + paramObj.wardId
                + "&streetId=" + paramObj.streetId,
                type: "PUT",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                data: vJsonUpdateData,
                success: function (res) {
                    console.log(res);
                    alert("successfully!")
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to update!");
                },
            });
        }

        // Hàm gọi lấy tất cả province
        function callApiGetProvince(){
            "use strict";
            $.ajax({
                url: gProvinceUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "province", gProjectObj.province.id);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả district by province id
        function callApiGetDistrictByProvinceId(paramProvinceId){
            "use strict";
            $.ajax({
                url: gDistrictUrl + "/province/" + paramProvinceId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "district", gProjectObj.district.id);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy ward by district id
        function callApiGetWardByDistrictId(paramDistrictId){
            "use strict";
            $.ajax({
                url: gWardUrl + "/district/" + paramDistrictId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "ward", gProjectObj.ward.id);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả street by district id
        function callApiGetStreetByDistrictId(paramDistrictId){
            "use strict";
            $.ajax({
                url: gStreetUrl + "/district/" + paramDistrictId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "street", gProjectObj.street.id);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả contractor
        function callApiGetContractor(){
            "use strict";
            $.ajax({
                url: gContractorUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "contractor");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả design unit
        function callApiGetDesignUnit(){
            "use strict";
            $.ajax({
                url: gDesignUnitUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "design-unit");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả investor
        function callApiGetInvestor(){
            "use strict";
            $.ajax({
                url: gInvestorUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "investor");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả region link
        function callApiGetRegionLink(){
            "use strict";
            $.ajax({
                url: gRegionLinkUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "region-link");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả region link
        function callApiGetUtilities(){
            "use strict";
            $.ajax({
                url: gUtilitiesUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "utilities");
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // Hàm tải dữ liệu vào form
        function loadDataToForm(paramObj){
            "use strict"
            $("#inp-id").val(paramObj.id);
            $("#inp-name").val(paramObj.name);
            $("#inp-acreage").val(paramObj.acreage);
            $("#inp-address").val(paramObj.address);
            $("#inp-num-apartment").val(paramObj.numApartment);
            $("#inp-num-block").val(paramObj.numBlock);
            $("#inp-num-floors").val(paramObj.numFloors);
            $("#inp-description").val(paramObj.description);
            $("#inp-slogan").val(paramObj.slogan);
            $("#inp-construct-area").val(paramObj.constructArea);
            $("#inp-apartmentt-area").val(paramObj.apartmenttArea);
            $("#inp-photo").val(paramObj.photo);
            $("#inp-lat").val(paramObj.lat);
            $("#inp-lng").val(paramObj.lng);


            var vSelectBox = $("#select-province");
            var bOption = $("<option>");
                bOption.prop("value", paramObj.province.id);
                bOption.prop("text", paramObj.province.name);
                vSelectBox.append(bOption);
                $("#select-province").val(paramObj.province.id);

            var vSelectBox = $("#select-district");
            var bOption = $("<option>");
                bOption.prop("value", paramObj.district.id);
                bOption.prop("text", paramObj.district.name);
                vSelectBox.append(bOption);
                $("#select-district").val(paramObj.district.id);

            var vSelectBox = $("#select-ward");
            var bOption = $("<option>");
                bOption.prop("value", paramObj.ward.id);
                bOption.prop("text", paramObj.ward.name);
                vSelectBox.append(bOption);
                $("#select-ward").val(paramObj.ward.id);
            var vSelectBox = $("#select-street");
            var bOption = $("<option>");
                bOption.prop("value", paramObj.street.id);
                bOption.prop("text", paramObj.street.name);
                vSelectBox.append(bOption);
                $("#select-street").val(paramObj.street.id);

            var vSelectBox = $("#select-contractor");
            var bOption = $("<option>");
                bOption.prop("value", paramObj.constructionContractor.id);
                bOption.prop("text", paramObj.constructionContractor.name);
                vSelectBox.append(bOption);
                $("#select-contractor").val(paramObj.constructionContractor.id);

            var vSelectBox = $("#select-design-unit");
            var bOption = $("<option>");
                bOption.prop("value", paramObj.designUnit.id);
                bOption.prop("text", paramObj.designUnit.name);
                vSelectBox.append(bOption);
                $("#select-design-unit").val(paramObj.designUnit.id);

            var vSelectBox = $("#select-investor");
            var bOption = $("<option>");
                bOption.prop("value", paramObj.investor.id);
                bOption.prop("text", paramObj.investor.name);
                vSelectBox.append(bOption);
                $("#select-investor").val(paramObj.investor.id);

            var vSelectBox = $("#select-region-link");
            var bOption = $("<option>");
                bOption.prop("value", paramObj.regionLink.id);
                bOption.prop("text", paramObj.regionLink.name);
                vSelectBox.append(bOption);
                $("#select-region-link").val(paramObj.regionLink.id);

            var vSelectBox = $("#select-utilities");
            var bOption = $("<option>");
                bOption.prop("value", paramObj.utilities.id);
                bOption.prop("text", paramObj.utilities.name);
                vSelectBox.append(bOption);
                $("#select-utilities").val(paramObj.utilities.id);
        }
    
        // Hàm thu thập dữ liệu
        function readDataForm(paramObj){
            "use strict"
            paramObj.name = $("#inp-name").val();
            paramObj.acreage = $("#inp-acreage").val();
            paramObj.address = $("#inp-address").val();
            paramObj.numApartment = $("#inp-num-apartment").val();
            paramObj.numBlock = $("#inp-num-block").val();
            paramObj.numFloors = $("#inp-num-floors").val();
            paramObj.description = $("#inp-description").val();
            paramObj.slogan = $("#inp-slogan").val();
            paramObj.constructArea = $("#inp-construct-area").val();
            paramObj.apartmenttArea = $("#inp-apartmentt-area").val();
            paramObj.photo = $("#inp-photo").val();
            paramObj.lat = $("#inp-lat").val();
            paramObj.lng = $("#inp-lng").val();

            paramObj.provinceId = $("#select-province :selected" ).val();
            paramObj.districtId = $("#select-district :selected" ).val();
            paramObj.wardId = $("#select-ward :selected" ).val();
            paramObj.streetId = $("#select-street :selected" ).val();
            paramObj.contractorId = $("#select-contractor :selected" ).val();
            paramObj.designUnitId = $("#select-design-unit :selected" ).val();
            paramObj.investorId = $("#select-investor :selected" ).val();
            paramObj.regionLinkId = $("#select-region-link :selected" ).val();
            paramObj.utilitiesId = $("#select-utilities :selected" ).val();
        }

        // Hàm tải dữ liệu province lên form
        function loadDataToSelectBox(paramArr, paramName, paramId){
            "use strict"
            var vSelectBox = $("#select-"+ paramName +"");

            // var bOption = $("<option>");
            //         bOption.prop("value", "");
            //         bOption.prop("text", "Select " + paramName);
            //         vSelectBox.append(bOption);

            for(var bI = 0; bI < paramArr.length; bI ++){
                if (paramArr[bI].id != paramId){
                    var bOption = $("<option>");
                    bOption.prop("value", paramArr[bI].id);
                    bOption.prop("text", paramArr[bI].name);
                    vSelectBox.append(bOption);
                }
            }
        }

        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    });