$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");
        var gUrl = new URL(window.location.href);
    
        var gId = gUrl.searchParams.get("id");

        var gUrl = "http://localhost:8080/realestate/real_estates";
        var gProvinceUrl = "http://localhost:8080/realestate/api/test/provinces"
        var gDistrictUrl = "http://localhost:8080/realestate/districts"
        var gWardUrl = "http://localhost:8080/realestate/wards"
        var gStreetUrl = "http://localhost:8080/realestate/streets"
        var gProjectUrl = "http://localhost:8080/realestate/projects"

        var gRealEstateObj = "";
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();

        $(document).on("click", "#btn-update", function(){
            onBtnUpdateClick();
        })

        $("#inp-photo").change(function(e) {
            $("#photo-upload").attr("src","");
            for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
        
                var file = e.originalEvent.srcElement.files[i];
        
                var img = $("#photo-upload");

                var reader = new FileReader();
                    reader.onloadend = function() {
                    img.attr("src", reader.result);
                }
                reader.readAsDataURL(file);
                $("#upload-photo").html(img);
            }
        });

        $(document).on("change", "#select-province", function(){
            var vNewProvinceId = $("#select-province :selected").val();
            $("#select-district").empty();
            $("#select-ward").empty();
            $("#select-street").empty();
            $("#select-ward").attr("disabled", true);
            $("#select-street").attr("disabled", true);
            callApiGetDistrictByProvinceId(vNewProvinceId);
        })
    
        $(document).on("change", "#select-district", function(){
            var nNewDistrictId = $("#select-district :selected").val();
            console.log(nNewDistrictId);
            $("#select-ward").empty();
            $("#select-street").empty();
            $("#select-ward").attr("disabled", false);
            $("#select-street").attr("disabled", false);
            callApiGetWardByDistrictId(nNewDistrictId);
            callApiGetStreetByDistrictId(nNewDistrictId);
        })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiGetRealEstateById(gId);
        }
    
        //Hàm xử lí sự kiện nhất nút cập nhật
        function onBtnUpdateClick(){
            "use strict"
            // khai báo dữ liệu cập nhật
            var vDataUpdate = {
                type: "",
                request: "",
                address: "",
                title: "",
                price: "",
                priceMin: "",
                acreage: "",
                direction: "",
                total_floors: "",
                number_floors: "",
                bath: "",
                apart_code: "",
                wall_area: "",
                bedroom: "",
                balcony: "",
                landscape_view: "",
                apart_loca: "",
                apart_type: "",
                furniture_type: "",
                price_rent: "",
                legal_doc: "",
                description: "",
                width_y: "",
                long_x: "",
                street_house: "",
                lat: "",
                lng: "",
                provinceId: "",
                districtId: "",
                wardId: "",
                streetId: "",
                projectId: "",
                accepted: "",
                photo: ""
            }
            //B1: Thu thập dữ liệu
            readDataForm(vDataUpdate);
            console.log(vDataUpdate);
            //B2: Kiểm tra dữ liệu(bỏ qua)
            //B3: Cập nhật dữ liệu
            callApiUpdateRealEstate(vDataUpdate);
    
        }
        // Hàm gọi api lấy address map by id
        function callApiGetRealEstateById(paramId) {
            "use strict";
            $.ajax({
                url: gUrl + "/" + paramId,
                type: "GET",
                dataType: "json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                async:false,
                success: function (res) {
                    console.log(res);
                    gRealEstateObj = res;
                    callApiGetProvince();
                    callApiGetDistrictByProvinceId(res.province.id);
                    callApiGetWardByDistrictId(res.district.id);
                    callApiGetStreetByDistrictId(res.district.id);
                    callApiGetProject(res);
                },
                error: function (err) {
                    console.log(err.responseText);
                    alert(err.responseText);
                    window.location.href = "../home-seller-manage.html"
                },
            });
            loadDataToForm(gRealEstateObj);
        }
        // Hàm gọi api cập nhật project map by id
        function callApiUpdateRealEstate(paramObj) {
            "use strict";
            var vJsonUpdateData = JSON.stringify(paramObj);
            $.ajax({
                url: gUrl + "/seller-role/" + gId 
                + "?projectId=" + paramObj.projectId
                + "&provinceId=" + paramObj.provinceId
                + "&districtId=" + paramObj.districtId
                + "&wardId=" + paramObj.wardId
                + "&streetId=" + paramObj.streetId,
                type: "PUT",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                data: vJsonUpdateData,
                success: function (res) {
                    console.log(res);
                    alert("successfully!")
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to update!");
                },
            });
        }

        // Hàm gọi lấy tất cả province
        function callApiGetProvince(){
            "use strict";
            $.ajax({
                url: gProvinceUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "province", gRealEstateObj.province.id);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả district by province id
        function callApiGetDistrictByProvinceId(paramId){
            "use strict";
            $.ajax({
                url: gDistrictUrl + "/province/" + paramId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "district", gRealEstateObj.district.id);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy ward by district id
        function callApiGetWardByDistrictId(paramId){
            "use strict";
            $.ajax({
                url: gWardUrl + "/district/" + paramId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "ward", gRealEstateObj.ward.id);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả street by district id
        function callApiGetStreetByDistrictId(paramId){
            "use strict";
            $.ajax({
                url: gStreetUrl + "/district/" + paramId,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "street", gRealEstateObj.street.id);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        // Hàm gọi lấy tất cả design unit
        function callApiGetProject(paramRes){
            "use strict";
            $.ajax({
                url: gProjectUrl,
                type: "GET",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                success: function (res) {
                    console.log(res);
                    loadDataToSelectBox(res, "project", paramRes.project.id);
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to get data!");
                },
            });
        }
        
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
        // Hàm tải dữ liệu vào form
        function loadDataToForm(paramObj){
            "use strict"
            $("#inp-id").val(paramObj.id);
            $("#inp-employee").val(paramObj.employee.firstName + " " + paramObj.employee.lastName);
            $("#select-type").val(paramObj.type);
            $("#select-request").val(paramObj.request);
            $("#inp-address").val(paramObj.address);
            $("#inp-title").val(paramObj.title);
            $("#inp-price").val(paramObj.price);
            $("#inp-price-min").val(paramObj.priceMin);
            $("#inp-acreage").val(paramObj.acreage);
            $("#select-direction").val(paramObj.direction);
            $("#inp-total-floors").val(paramObj.total_floors);
            $("#inp-number-floors").val(paramObj.number_floors);
            $("#inp-bath").val(paramObj.bath);
            $("#inp-apart-code").val(paramObj.apart_code);
            $("#inp-wall-area").val(paramObj.wall_area);
            $("#inp-bed-room").val(paramObj.bedroom);
            $("#select-balcony").val(paramObj.balcony);
            $("#inp-landscape-view").val(paramObj.landscape_view);
            $("#select-apart-local").val(paramObj.apart_loca);
            $("#select-apart-type").val(paramObj.apart_type);
            $("#select-furniture-type").val(paramObj.furniture_type);
            $("#inp-price-rent").val(paramObj.price_rent);
            $("#inp-legal-doc").val(paramObj.legal_doc);
            $("#inp-description").val(paramObj.description);
            $("#inp-width-y").val(paramObj.width_y);
            $("#inp-long-x").val(paramObj.long_x);
            $("#inp-street-house").val(paramObj.street_house);
            $("#inp-lat").val(paramObj.lat);
            $("#inp-lng").val(paramObj.lng);
            $("#photo-upload").attr("src", paramObj.photo);


            if(paramObj.province != null){
                var vSelectBox = $("#select-province");
                var bOption = $("<option>");
                bOption.prop("value", paramObj.province.id);
                bOption.prop("text", paramObj.province.name);
                vSelectBox.append(bOption);
                $("#select-province").val(paramObj.province.id);
            }

            if(paramObj.district != null){
                var vSelectBox = $("#select-district");
                var bOption = $("<option>");
                bOption.prop("value", paramObj.district.id);
                bOption.prop("text", paramObj.district.name);
                vSelectBox.append(bOption);
                $("#select-district").val(paramObj.district.id);
            }

            if(paramObj.ward != null){
                var vSelectBox = $("#select-ward");
                var bOption = $("<option>");
                bOption.prop("value", paramObj.ward.id);
                bOption.prop("text", paramObj.ward.name);
                vSelectBox.append(bOption);
                $("#select-ward").val(paramObj.ward.id);
            }

            if (paramObj.street != null){
                var vSelectBox = $("#select-street");
                var bOption = $("<option>");
                bOption.prop("value", paramObj.street.id);
                bOption.prop("text", paramObj.street.name);
                vSelectBox.append(bOption);
                $("#select-street").val(paramObj.street.id);
            }

            if (paramObj.project != null){
                var vSelectBox = $("#select-project");
                var bOption = $("<option>");
                bOption.prop("value", paramObj.project.id);
                bOption.prop("text", paramObj.project.name);
                vSelectBox.append(bOption);
                $("#select-project").val(paramObj.project.id);
            }

            if(paramObj.accepted == true){
                $("#select-accepted").val("true");
            }
            if(paramObj.accepted == false){
                $("#select-accepted").val("false");
            }

        }
    
        // Hàm thu thập dữ liệu
        function readDataForm(paramObj){
            "use strict"
            paramObj.id = $("#inp-id").val();
            paramObj.type = $("#select-type").val();
            paramObj.request = $("#select-request").val();
            paramObj.address = $("#inp-address").val();
            paramObj.title = $("#inp-title").val();
            paramObj.price = $("#inp-price").val();
            paramObj.priceMin = $("#inp-price-min").val();
            paramObj.acreage = $("#inp-acreage").val();
            paramObj.direction = $("#select-direction").val();
            paramObj.total_floors = $("#inp-total-floors").val();
            paramObj.number_floors = $("#inp-number-floors").val();
            paramObj.bath = $("#inp-bath").val();
            paramObj.apart_code = $("#inp-apart-code").val();
            paramObj.wall_area = $("#inp-wall-area").val();
            paramObj.bedroom = $("#inp-bed-room").val();
            paramObj.balcony = $("#select-balcony").val();
            paramObj.landscape_view = $("#inp-landscape-view").val();
            paramObj.apart_loca = $("#select-apart-local").val();
            paramObj.apart_type = $("#select-apart-type").val();
            paramObj.furniture_type = $("#select-furniture-type").val();
            paramObj.price_rent = $("#inp-price-rent").val();
            paramObj.legal_doc = $("#inp-legal-doc").val();
            paramObj.description = $("#inp-description").val();
            paramObj.width_y = $("#inp-width-y").val();
            paramObj.long_x = $("#inp-long-x").val();
            paramObj.street_house = $("#inp-street-house").val();
            paramObj.lat = $("#inp-lat").val();
            paramObj.lng = $("#inp-lng").val();


            paramObj.provinceId = $("#select-province :selected" ).val();
            paramObj.districtId = $("#select-district :selected" ).val();
            paramObj.wardId = $("#select-ward :selected" ).val();
            paramObj.streetId = $("#select-street :selected" ).val();
            paramObj.projectId = $("#select-project :selected" ).val();
            paramObj.accepted = $("#select-accepted :selected" ).val();
            paramObj.photo = $("#photo-upload").attr("src");
        }

        // Hàm tải dữ liệu lên form
        function loadDataToSelectBox(paramArr, paramName, paramId){
            "use strict"
            var vSelectBox = $("#select-"+ paramName +"");

            for(var bI = 0; bI < paramArr.length; bI ++){
                if (paramArr[bI].id != paramId){
                    var bOption = $("<option>");
                    bOption.prop("value", paramArr[bI].id);
                    bOption.prop("text", paramArr[bI].name);
                    vSelectBox.append(bOption);
                }
            }
        }
        // Hàm tải dữ liệu lên form
        function loadCustomerToSelectBox(paramArr, paramId){
            "use strict"
            var vSelectBox = $("#select-customer");

            for(var bI = 0; bI < paramArr.length; bI ++){
                if (paramArr[bI].id != paramId){
                    var bOption = $("<option>");
                    bOption.prop("value", paramArr[bI].id);
                    bOption.prop("text", paramArr[bI].contactName);
                    vSelectBox.append(bOption);
                }
            }
        }

        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    });