$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
        const vtoken = getCookie("token");
        var gUrl = "http://localhost:8080/realestate/address_map";
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    
        $(document).on("click", "#btn-add", function(){
            onBtnAddClick();
        })
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    
        //Hàm xử lí sự kiện nhất nút cập nhật
        function onBtnAddClick(){
            "use strict"
            // khai báo dữ liệu cập nhật
            var vDataUpdate = {
                address: "",
                lat: "",
                lng: "",
            }
            //B1: Thu thập dữ liệu
            readDataForm(vDataUpdate);
            console.log(vDataUpdate);
            //B2: Kiểm tra dữ liệu(bỏ qua)
            //B3: Cập nhật dữ liệu
            callApiAddAddressMap(vDataUpdate);
    
        }
        // Hàm gọi api cập nhật address map by id
        function callApiAddAddressMap(paramObj) {
            "use strict";
            var vJsonUpdateData = JSON.stringify(paramObj);
            $.ajax({
                url: gUrl,
                type: "POST",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + vtoken + "" 
                  },
                data: vJsonUpdateData,
                success: function (res) {
                    console.log(res);
                    alert("successfully!")
                },
                error: function (err) {
                    console.log(err.responseText);
                    console.log("Fail to create!");
                },
            });
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    
        // Hàm thu thập dữ liệu
        function readDataForm(paramObj){
            "use strict"
            paramObj.address = $("#inp-address").val();
            paramObj.lat = $("#inp-lat").val();
            paramObj.lng = $("#inp-lng").val();
        }

        //Hàm get Cookie đã giới thiệu ở bài trước
        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    });